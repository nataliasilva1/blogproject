import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"


const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Olá</h1>
    <p>Bem Vindo ao meu novo Site!</p>
    <p>Projetinho pessoal para estudo.</p>
    <div style={{ maxWidth: `500px`, marginBottom: `1.45rem`, justifyContent:`center`}}>
      <Image />
    </div>
    <Link to="/page-2/">Ir para página 2</Link> <br />
    <Link to="/blog/my-first-post">Post usando Markdown!</Link>
  </Layout>
)

export default IndexPage
