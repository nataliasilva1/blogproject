import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

const SecondPage = () => (
  <Layout>
    <SEO title="Page two" />
    <h1>Oie, você está na segunda página!</h1>
    <p>Seja bem vindo!</p>
    <Link to="/">Voltar a página Início</Link>
  </Layout>
)

export default SecondPage
